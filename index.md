---
layout: home
---

"Anahtarı" is a Turkish word which means "Key" that we open the door. Its etymology is the greek word "ανοιχτήρι" which is a tool that we open something e.g. a can or a bootle of beer.
